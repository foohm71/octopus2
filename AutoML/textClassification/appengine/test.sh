curl -X POST -H "Content-Type: application/json" \
	-d '{"title": "TestSizeBasedThrottler fails occasionally", "description": "Every now and then TestSizeBasedThrottler fails reaching the test internal timeouts.I think the timeouts (200ms) are too short for the Jenkins machines.On my (reasonably fast) machine I get timeout reliably when I set the timeout to 50ms, and occasionally at 100ms."}' \
	"http://localhost:8080/predict"

