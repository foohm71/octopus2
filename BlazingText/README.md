# Introduction

In the main Octopus 2 repository, we have the notebooks and code to build, analyze and deploy the 1D CNN based classifier for Jira tickets (classifying them based on bug priority). In this repository, we have the notebooks and code to build, analyze and deploy a word2vec based classifier using Amazon's Sagemaker's BlazingText (hence the name) built in text classification engine. 

There are essentially 2 ways to build and deploy a Amazon Sagemaker built in algorithm:
1.  Using the Amazon Sagemaker UI
2.  Using a Jupyter notebook on Amazon Sagemaker Jupyter Notebook instance

In this project we have both. 

# A Little about word2vec

I do not intend to cover too much about word2vec as I would not do justice to the topic but suffice to say, word2vec is a DNN approach to creating a word embedding. Word2Vec was mentioned in my Medium article on the Octopus 2 project as one of the approaches to use to perform text classification - see https://levelup.gitconnected.com/building-a-deployable-jira-bug-classification-engine-using-tensorflow-745667c72607

If you would like to know more about word2vec, I strongly recommend watching "Word2Vec Explained!" by the Python Programmer on YouTube (https://youtu.be/yFFp9RYpOb0). In essence, word2vec is a DNN that given a bunch of text, maps words to a N-dimensional vector space. The idea behind that is that words that are "closer" to each other "belong" to each other. Usually that is based on the distance they are between each other in the sentences of the bunch of text it is trained on. We can then use this "closeness" to train a classification algorithm.

There are 2 parts to this project: 
1.  The supervised part
2.  The word2vec or unsupervised part

# The supervised part

In this part (folder named ```supervised```, duh!), I make use of the Amazon Sagemaker tool chain to build, analyze and deploy a Jira bug classification engine using the BlazingText built in algorithm. The dataset used is the ```JIRA_OPEN_DATA_LARGESET_PROCESSED.csv```. It is the same one used as the DNNModelAnalysis.ipynb in the Octopus 2 folder as I wanted to compare the results with the 1D CNN approach. 

In here there are 2 notebooks:
1. BlazingTextDataPrepTrainEval.ipynb
2. blazingtext_text_classification_jira.ipynb

## BlazingTextDataPrepTrainEval.ipynb

This notebook is meant to run on Google Colab (why cos Amazon Sagemaker costs money to run). This notebook covers the following:

1. How to generate the training and validation data in the form needed by BlazingText 
2. How to go about training the model, starting with uploading the data files to S3
3. How to evaluate the model using CloudWatch logs
4. How to create an model, model endpoint, lambda, API gateway to deploy the model
5. How to test the API

## blazingtext_text_classification_jira.ipynb

This notebook was created as I wanted to be able to evaluate the trained model ie. get precision, recall metrics and confusion matrix and the Sagemaker toolchain did not have an easy way to get them so I had to DIY. This notebook has to be run on Amazon Sagemaker notebook instance as it uses Amazon APIs (eg. sagemaker, boto3)

This notebook is modified from an example provided by Amazon and it covers the following
1. How to train the model via Amazon Sagemaker Jupyter notebook
2. How to deploy a model endpoint in the notebook
3. How to make predictions using the endpoint from the notebook
4. How to delete the endpoint (as it costs money to run)

The parts that were added by me were:
1. How to evalute the model using the test data set that was created from the ```JIRA_OPEN_DATA_LARGESET_PROCESSED.csv``` dataset
2. How to DIY create a confusion matrix and calculate the accuracy, precision and recall metrics 
3. How to evaluate the model using an entirely different data set, the ```JIRA_OPEN_DATA_ZOOKEEPER.csv``` dataset



# The word2vec or unsupervised part

The folder name is ```word2vec```. There is only 1 notebook there ```Word2Vec_tSNE_Analysis.ipynb```. In this notebook I am using the word2vec feature in BlazingText to generate the word embedding file ```vectors.txt```. Next I use the gensim and matplotlib libraries to generate a t-SNE plot of the words extracted by BlazingText to see how they cluster. 

This is still a work in progress. 

## A little about t-SNE

Again I would do severe injustice to the explanation but at a high level, t-SNE is similar to PCA in that it performs dimentionality reduction. It tries to preserve the "closeness" of the words by mapping them to the (in this case) 2D space by preserving the clusters. For a really good explanation of t-SNE, watch the Stat Quest episode on t-SNE (https://youtu.be/NEaUSP4YerM)